const { defaultResponse, responseTexts } = require('../contants')
const { todoService } = require('../service')
const { getCurrentFilePath } = require('../utils')

const filePath = getCurrentFilePath(__filename)

const getAllTodos = async (req, res) => {
  const response = Object.assign(defaultResponse, {})
  try {
    const resp = await todoService.getAllTodos()
    response.status = 200
    response.message = responseTexts.todo.allFetched
    response.body = resp
  } catch (err) {
    console.log(`Error in ${filePath}/getAllTodos: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

const getTodoById = async (req, res) => {
  const response = Object.assign(defaultResponse, {})
  try {
    const resp = await todoService.getTodoById(req.params.id)
    response.status = 200
    response.message = responseTexts.todo.oneFetched
    response.body = resp
  } catch (err) {
    console.log(`Error in ${filePath}/getTodoById: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

const createTodo = async (req, res) => {
  const response = Object.assign(defaultResponse, {})
  try {
    const resp = await todoService.createTodo(req.body)
    response.status = 201
    response.message = responseTexts.todo.created
    response.body = resp
  } catch (err) {
    console.log(`Error in ${filePath}/createTodo: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

const deleteTodo = async (req, res) => {
  const response = Object.assign(defaultResponse, {})
  try {
    const resp = await todoService.deleteTodo(req.params.id)
    response.status = 200
    response.message = responseTexts.todo.deleted
    response.body = resp
  } catch (err) {
    console.log(`Error in ${filePath}/deleteTodo: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

const updateTodo = async (req, res) => {
  const response = Object.assign(defaultResponse, {})
  try {
    const resp = await todoService.updateTodo(req.params.id, req.body)
    response.status = 200
    response.message = responseTexts.todo.updated
    response.body = resp
  } catch (err) {
    console.log(`Error in ${filePath}/updateTodo: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

module.exports = {
  getAllTodos,
  getTodoById,
  createTodo,
  deleteTodo,
  updateTodo
}
