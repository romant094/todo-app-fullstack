const path = require('node:path')
require('dotenv').config({ path: path.resolve(__dirname, '../../../.env') })

const express = require('express')
const { createProxyMiddleware } = require('http-proxy-middleware')
const { readFileSync } = require('node:fs')

const dbConnect = require('./db')
const { port, isDev, apiBase } = require('./env')
const { clientPathHtml, clientPathFolder } = require('./contants')
const { todosRouter } = require('./router')

void dbConnect()

const app = express()
app.use(express.json())
app.use(express.urlencoded())

app.use(`${apiBase}/todos`, todosRouter)

if (isDev) {
  app.use(
    '/',
    createProxyMiddleware({
      target: 'http://localhost:3000',
      changeOrigin: true
    })
  )
} else {
  app.use(express.static(clientPathFolder))
  const html = readFileSync(path.resolve(clientPathHtml), 'utf-8')

  app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'text/html')
    res.sendFile(html)
  })
}

app.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})
