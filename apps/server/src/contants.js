const path = require('node:path')

const pathToClientDist = '../node_modules/todo-app-client/dist'
const clientPathFolder = path.join(__dirname, pathToClientDist)
const clientPathHtml = path.join(__dirname, `${pathToClientDist}/index.html`)

const defaultResponse = {
  status: 400,
  message: '',
  body: {}
}

const responseTexts = {
  todo: {
    created: 'Todo was successfully created',
    updated: 'Todo was successfully updated',
    deleted: 'Todo was successfully deleted',
    allFetched: 'Todos fetched successfully',
    oneFetched: 'Todo fetched successfully',
    notFound: 'Todo not found'
  }
}

module.exports = {
  clientPathFolder,
  clientPathHtml,
  defaultResponse,
  responseTexts
}
