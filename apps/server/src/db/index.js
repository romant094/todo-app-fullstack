const mongoose = require('mongoose')
const { dbUrl } = require('../env')

module.exports = async () => {
  try {
    await mongoose.connect(dbUrl, { useNewUrlParser: true })
    console.info('Database connected')
  } catch (err) {
    console.log(`Database connection error: ${err}`)
    throw new Error(err)
  }
}
