const port = process.env.PORT || 3100
const apiBase = process.env.API_BASE || '/api/v1'
const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017/todo-app'
const isDev = process.env.NODE_ENV === 'development'
const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  port,
  apiBase,
  dbUrl,
  isDev,
  isProd
}
