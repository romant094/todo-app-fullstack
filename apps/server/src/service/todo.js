const { Todo } = require('../model')
const { formatMongoData, getCurrentFilePath } = require('../utils')

const filePath = getCurrentFilePath(__filename)

const createTodo = async (data) => {
  try {
    const todo = new Todo(data)
    const result = await todo.save()
    return formatMongoData(result)
  } catch (err) {
    console.log(`Error in ${filePath}/createTodo: ${err}`)
    throw new Error(err)
  }
}

const getTodoById = async (id) => {
  try {
    const result = await Todo.findById(id)
    return formatMongoData(result)
  } catch (err) {
    console.log(`Error in ${filePath}/getTodo: ${err}`)
    throw new Error(err)
  }
}

const updateTodo = async (id, data) => {
  try {
    const result = await Todo.findByIdAndUpdate(id, data, { new: true })
    return formatMongoData(result)
  } catch (err) {
    console.log(`Error in ${filePath}/updateTodo: ${err}`)
    throw new Error(err)
  }
}

const getAllTodos = async () => {
  try {
    const result = await Todo.find()
    return formatMongoData(result)
  } catch (err) {
    console.log(`Error in ${filePath}/getAllTodos: ${err}`)
    throw new Error(err)
  }
}

const deleteTodo = async (id) => {
  try {
    return await Todo.findByIdAndDelete(id)
  } catch (err) {
    console.log(`Error in ${filePath}/deleteTodo: ${err}`)
    throw new Error(err)
  }
}

module.exports = {
  createTodo,
  getTodoById,
  updateTodo,
  getAllTodos,
  deleteTodo
}
