const { Router } = require('express')
const { todosController } = require('../controller')

const router = Router()

const { getAllTodos, getTodoById, createTodo, deleteTodo, updateTodo } = todosController

router.get('/', getAllTodos)
router.post('/', createTodo)
router.get('/:id', getTodoById)
router.delete('/:id', deleteTodo)
router.put('/:id', updateTodo)

module.exports = router
