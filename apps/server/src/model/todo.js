const mongoose = require('mongoose')

const schema = new mongoose.Schema(
  {
    label: String,
    done: Boolean
  },
  {
    timestamps: true,
    toObject: {
      transform: (_, ret) => {
        const { _id, __v, ...rest } = ret
        return { ...rest, id: ret._id }
      }
    }
  }
)

module.exports = mongoose.model('Todo', schema)
