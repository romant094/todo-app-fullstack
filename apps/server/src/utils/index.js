// const mongoose = require('mongoose')
// const constants = require('../constants')
const path = require('node:path')

const formatMongoData = (data) => {
  if (Array.isArray(data)) {
    return data.map((d) => d.toObject())
  }
  return data.toObject()
}

const getCurrentFilePath = (filename) => path.relative(process.cwd(), filename)

// const checkObjectId = id => {
//   if (!mongoose.isValidObjectId(id)) {
//     throw new Error(constants.databaseMessages.INVALID_ID)
//   }
// }

module.exports = {
  formatMongoData,
  getCurrentFilePath
  // checkObjectId
}
