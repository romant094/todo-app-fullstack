import * as path from 'node:path'
import react from '@vitejs/plugin-react'
import dotenv from 'dotenv'
import { defineConfig } from 'vite'

dotenv.config({ path: path.resolve(__dirname, '../../.env') })

export default defineConfig({
  plugins: [react()],
  define: {
    'process.env': process.env
  }
})
