import React from 'react'
import ReactDOM from 'react-dom/client'
import { App } from './App.jsx'

console.log('test env', import.meta.env.VITE_TEST)

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)
